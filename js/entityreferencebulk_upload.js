(function ($) {


    Drupal.behaviors.entityrerefencebulk_fileupload = {

        uploaders:[],

        attach:function (context, settings) {

            var target_context = Drupal.settings.entityreferencebulk.fileupload_target;
            var uploaders = this.uploaders;
            $(target_context + ' .plupload-element').each(function () {
                var uploader = $(this).pluploadQueue();
                uploaders.push(uploader);
                var status = $('#' + uploader.settings.container).parents().find('input.upload_widget_status')
                uploader.bind('UploadComplete', function (uploader, file, response) {
                    status.trigger("change");
                });
            })

        },

        detach:function (context, settings) {

            $(this.uploaders).each(function () {
                this.destroy();
            });

        }

    }

})(jQuery);


