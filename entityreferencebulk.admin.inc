<?php
/**
 * Created by IntelliJ IDEA.
 * User: martino
 * Date: 13/06/12
 * Time: 15.28
 * To change this template use File | Settings | File Templates.
 */

/**
 * Implements hook_field_widget_settings_form().
 */
function _entityreferencebulk_field_widget_settings_form($field, $instance) {

  $widget = $instance['widget'];

  if ($widget['type'] == 'entityreferencebulk') {

    $form = array();
    $defaults = field_info_widget_settings($widget['type']);
    $settings = array_merge($defaults, $widget['settings']);

    // Get all file fields of the referenced entity
    $bundle_file_fields = _entityreferencebulk_get_referenced_file_fields($field);

    // Get all entity reference fields of the referenced entity pointing back to
    // the this entity
    $bundle_reference_fields = _entityreferencebulk_get_entity_reference_fields($field);

    if (empty($bundle_file_fields)) {
      //TODO handle also referenced entities without file fields
      form_set_error(NULL, t('This widget can be used just if referenced bundles have at least one file field.'));

    }
    else {

      $target_type = $field['settings']['target_type'];
      $bundle_info = field_info_bundles($target_type);
      $bundle_properties = entity_get_property_info($field['settings']['target_type']);

      if ($target_type == 'node') {
        $node_types = node_type_get_types();
      }

      $form['entityreferencebulk'] = array(
        '#type' => 'fieldset',
        '#title' => t('Entity reference bulk configuration'),
        '#description' => t('Specify which file field to use for referenced entities bulk creation on multiple file upload.')
      );

      $styles = array();
      foreach (image_styles() as $name => $style) {
        $styles[$name] = $style['name'];
      }

      $default_image_style = drupal_array_get_nested_value($instance,
        array('widget', 'settings', 'entityreferencebulk', 'image_style'));

      // The image style to use on the selection / reorder table
      $form['entityreferencebulk']['image_style'] = array(
        '#type' => 'select',
        '#title' => t('Edit image style'),
        '#default_value' => !empty($default_image_style) ? $default_image_style : 'thumbnail',
        '#options' => $styles
      );

      // Settings can be different for each referenced entity bundle
      foreach ($bundle_file_fields as $bundle => $fields) {

        $form['entityreferencebulk']['bundles'][$bundle] = array(
          '#type' => 'fieldset',
          '#title' => t($bundle_info[$bundle]['label'] . ' reference configuration'),
          '#description' => t('Configure the file field to use for bulk upload and other fields available during referenced entities bulk creation.')
        );

        $options = array();
        foreach ($fields as $field_name) {
          $field = field_info_instance($target_type, $field_name, $bundle);
          $options[$field_name] = $field['label'];
        }

        // File field to use for the current bundle
        $default_file_field = drupal_array_get_nested_value($instance,
          array('widget', 'settings', 'entityreferencebulk', 'bundles', $bundle, 'file_field'));

        $form['entityreferencebulk']['bundles'][$bundle]['file_field'] = array(
          '#type' => 'select',
          '#title' => t('File field to use for bulk upload creation.'),
          '#options' => $options,
          '#required' => TRUE,
          '#default_value' => $default_file_field
        );

        // There exist fields in target bundle pointing back
        // to the entity we are adding the field to. Give the
        // opportunity to automatically manage the backlink during
        // bulk reference creation
        if (array_key_exists($bundle, $bundle_reference_fields)) {

          $backref_options = array();
          $fields = $bundle_reference_fields[$bundle];
          foreach ($fields as $field_name) {
            $backref_field = field_info_instance($target_type, $field_name, $bundle);
            $backref_options[$field_name] = $backref_field['label'];
          }

          $form['entityreferencebulk']['bundles'][$bundle]['backreference_field'] = array(
            '#type' => 'select',
            '#title' => t('Field to use for automatic backreferefence management.'),
            '#description' => t('If the target entity contains at least an entity reference
                                            field pointing back to the referencing entity it is possible to automatically
                                            manage this backreference on bulk creation.'),
            '#options' => $backref_options,
            '#required' => TRUE,
            '#empty_option' => t('Do not use automatic backreference management.'),
          );

        }

        // Decide which referenced entity properties should be available during
        // bulk edit/creation of referenced entities

        // Node title is not a field so it must be treated separately
        if ($target_type == 'node' && array_key_exists($bundle, $node_types)) {
          $type = $node_types[$bundle];
          if ($type->has_title) {
            $form['entityreferencebulk']['bundles'][$bundle]['fields']['title'] = array(
              '#type' => 'checkbox',
              '#title' => t($type->title_label),
              '#default_value' => '1',
              '#disabled' => TRUE,
            );
          }
        }

        // All other properties
        foreach ($bundle_properties['bundles'][$bundle]['properties'] as $key => $value) {

          $fragment = array(
            '#type' => 'checkbox',
            '#title' => t($value['label']),
            // Show the field as selected (and input disabled) if it's a required field
            '#default_value' => $value['required'] ? '1' : '0',
            '#disabled' => $value['required'] ? TRUE : FALSE,
          );

          if (in_array($key, $bundle_file_fields[$bundle])) {
            $fragment += array(
              '#states' => array(
                // Only show this field if it's not the main bulk reference creation file field
                'invisible' => array(
                  '#edit-instance-widget-settings-entityreferencebulk-bundles-' . $bundle
                    . '-file-field' => array('value' => $key),
                ),
              ),
            );
          }


          if (in_array($key, $bundle_reference_fields[$bundle])) {
            $fragment += array(
              '#states' => array(
                // Only show this field if it's not the automatically managed target backreference field.
                'invisible' => array(
                  '#edit-instance-widget-settings-entityreferencebulk-bundles-' . $bundle
                    . '-backreference-field' => array('value' => $key),
                ),
              ),
            );
          }


          $form['entityreferencebulk']['bundles'][$bundle]['fields'][$key] = $fragment;


        }


      }

    }

  }
  return $form;
}


/**
 * Given a entityreference field returns
 *
 * @param $field The entityreference field
 * @return array(<bundle> => array(<filefield_name1>, <fieldfieldname2>, ...), <bundle2> => array(...
 */
function _entityreferencebulk_get_referenced_file_fields($field) {

  $ref_file_fields = array();

  // It seems it's not possible to have different entities targets so
  // I just take the first target type
  $target_type = $field['settings']['target_type'];

  $ref_bundles = $field['settings']['handler_settings']['target_bundles'];


  $fields = field_info_fields();

  // Build up the array structure
  foreach ($fields as $field) {

    if (_entityreferencebulk_is_file_field($field['type'])
      && array_key_exists('bundles', $field)
      && array_key_exists($target_type, $field['bundles'])
      && is_array($field['bundles'][$target_type])
    ) {

      foreach ($field['bundles'][$target_type] as $bundle) {

        if (in_array($bundle, $ref_bundles)) {

          if (!array_key_exists($bundle, $ref_file_fields)) {
            $ref_file_fields[$bundle] = array();
          }
          array_push($ref_file_fields[$bundle], $field['field_name']);

        }

      }
    }
  }


  return $ref_file_fields;

}

/**
 * Given a entityreference field returns
 *
 * @param $field The entity_reference field
 * @return array(<bundle> => array(<entity_reference_field_name1>, <entity_reference_field_name2>, ...), <bundle2> => array(...
 */
function _entityreferencebulk_get_entity_reference_fields($field) {

  $ref_file_fields = array();

  // It seems it's not possible to have different entities targets so
  // I just take the first target type
  $target_type = $field['settings']['target_type'];

  $ref_bundles = $field['settings']['handler_settings']['target_bundles'];


  $fields = field_info_fields();

  // Build up the array structure
  foreach ($fields as $field) {

    if ($field['type'] == 'entityreference'
      && array_key_exists('bundles', $field)
      && array_key_exists($target_type, $field['bundles'])
      && is_array($field['bundles'][$target_type])
    ) {

      foreach ($field['bundles'][$target_type] as $bundle) {

        if (in_array($bundle, $ref_bundles)) {

          if (!array_key_exists($bundle, $ref_file_fields)) {
            $ref_file_fields[$bundle] = array();
          }
          array_push($ref_file_fields[$bundle], $field['field_name']);

        }

      }
    }
  }


  return $ref_file_fields;

}

/**
 * @param $type A string representing the field type
 * @return TRUE if the field is considered a file type, FALSE otherwise
 */
function _entityreferencebulk_is_file_field($type) {

  if ($type == 'file' || $type == 'image') {
    return TRUE;
  }

  return FALSE;

}